import {host, role} from './settingDebug'

function login(){
    const boxlog=document.querySelector("#debug-bar-dropdown-log-content")
    const opensession=document.createElement('form')
    opensession.method='post'
    const selectrole=document.createElement('select')
    selectrole.name="selectrole"
    const roles=role()
    roles.forEach((r)=>{
        const choixrole=document.createElement('option')
        choixrole.value=r
        choixrole.innerText=r
        selectrole.appendChild(choixrole)
    })
    const connexion=document.createElement('input')
    connexion.type='submit'
    connexion.name='connexion'
    connexion.value='Connexion'
    connexion.classList.add("btn-log")
    opensession.appendChild(selectrole)
    opensession.appendChild(connexion)
    if (boxlog){
        boxlog.append(opensession)
    }

    async function getSession() {
        let params = new FormData();
        params.append('role', selectrole.value)
        const res = await fetch(host() + '/login', {
            body: params,
            method: 'post',
        });
        const data = await res.json();
        window.location.href=window.location.href
    }

    opensession.addEventListener('submit',(evt)=>{
        evt.preventDefault()
         getSession()
    })
}
function logout()
{
    async function closeSession() {
        let params = new FormData();
        const res = await fetch(host() + '/logout', {
            body: params,
            method: 'post',
        });
        window.location.href=window.location.href
    }

    const boxlog=document.querySelector("#debug-bar-dropdown-log-content")
    const closersession=document.createElement('div')
    closersession.innerText='Déconnexion'
    closersession.classList.add("btn-log")
    closersession.classList.add("btn-log-deco")
    if (boxlog){
        boxlog.append(closersession)
    }

    closersession.addEventListener('click',(evt)=>
    {
        evt.preventDefault()
        closeSession()
    })
}
export function getinfolog()
{
    login()
    logout()
}
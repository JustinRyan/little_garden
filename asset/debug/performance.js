let startLoadTime = new Date().getTime();

async function getServorTime() {
    let startTime = performance.now();
    return fetch('votre-url')
        .then(response => {
            return performance.now() - startTime;
        })
}

function getNumberofMedia(){
    let images = document.getElementsByTagName('img').length;
    let iframes = document.getElementsByTagName('iframe');

    let videosInFrames = 0;
    for (let i = 0; i < iframes.length; i++) {
        let iframeSrc = iframes[i].src;

        if (iframeSrc) {
            if (iframeSrc.includes('youtube.com') || iframeSrc.includes('dailymotion.com') || iframeSrc.includes('odysee.com'))
                videosInFrames++;
        }
    }
    return {'image': images, 'video': videosInFrames,}
}


export async function getPerformance(){
    const performanceBox = document.querySelector('#debug-bar-dropdown-performance-content')

    let loadTime = new Date().getTime() - startLoadTime;
    let servorTime = await getServorTime()
    let media = getNumberofMedia()

    const ul = document.createElement('ul')
    const licharg = document.createElement('li')
    const liserv = document.createElement('li')
    const liimg = document.createElement('li')
    const livideo = document.createElement('li')

    ul.id="listePerformance"
    ul.classList.add('liste-element')
    licharg.classList.add('element')
    liserv.classList.add('element')
    liimg.classList.add('element')
    livideo.classList.add('element')

    ul.innerText = 'Performance: '
    licharg.innerText = 'Temps de chargement de la page: '+ loadTime.toFixed(2) + ' ms'
    liserv.innerText = 'Temps de réponse du serveur: '+ servorTime.toFixed(2) + ' ms'
    liimg.innerText = 'Nombre d\'image: '+media['image']
    livideo.innerText = 'Nombre de vidéo: '+media['video']

    ul.appendChild(licharg)
    ul.appendChild(liserv)
    ul.appendChild(liimg)
    ul.appendChild(livideo)
    if (performanceBox){
        performanceBox.append(ul)
    }
}

